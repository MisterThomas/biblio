package com.oc.bibliotheque.livre.livrereservation.controller;

import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import com.oc.bibliotheque.livre.livrereservation.model.Livre;
import com.oc.bibliotheque.livre.livrereservation.repository.EmpruntRepository;
import com.oc.bibliotheque.livre.livrereservation.repository.LivreRepository;
import com.oc.bibliotheque.livre.livrereservation.service.LivreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LivreController {

    private static final Logger logger = LoggerFactory.getLogger(LivreController.class);


    @Autowired
    private LivreService livreService;

    @Autowired
    LivreRepository livreRepository;

    @Autowired
    EmpruntRepository empruntRepository;

    @GetMapping("/api/livre/listelivres")
    public List<Livre> listeLivres(){
        List<Livre> livres = livreRepository.findAll();
        return livres;
    }



    @GetMapping("/api/livre/test")
    public List<Livre> livres(String username){
        List<Livre> livres = livreService.getLivrebyLivre(username);
        return livres;
    }



    @PostMapping("/api/livre/listelivres/recherche")
    @ResponseBody
    public List<Livre> listeLivresAuteur(@RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre){
    //    auteur = "anthony horowtiz";
   //     titre = "l ile du crane";
        List<Livre> livres = livreRepository.findByAuteurOrTitre(auteur, titre);
        return livres;
    }




    @GetMapping(value="/api/Livre/{id}")
    public Optional<Livre> detailLivre(@PathVariable long id){
        return livreService.getLivreById(id);
    }



}
