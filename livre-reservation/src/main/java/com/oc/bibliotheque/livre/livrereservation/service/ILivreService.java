package com.oc.bibliotheque.livre.livrereservation.service;

import com.oc.bibliotheque.livre.livrereservation.model.Livre;

import java.util.List;
import java.util.Optional;

public interface ILivreService {

    List<Livre> getLivrebyLivre(String livre);

    List<Livre> getLivrebyTitre(String titre);

    Optional<Livre> getLivreById(long id);

  void addLivre(String user, String auteur, String titre, int nombre_disponible, boolean reservation_disponible, boolean isDone);


    void saveLivre(Livre livre);

}
