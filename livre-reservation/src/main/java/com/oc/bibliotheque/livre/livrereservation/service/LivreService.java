package com.oc.bibliotheque.livre.livrereservation.service;

import com.oc.bibliotheque.livre.livrereservation.model.Livre;
import com.oc.bibliotheque.livre.livrereservation.repository.LivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LivreService implements ILivreService {


    @Autowired
    LivreRepository livreRepository;


    @Override
    public List<Livre> getLivrebyLivre(String livre) {
        return livreRepository.findByUsername(livre);
    }

    @Override
    public List<Livre> getLivrebyTitre(String titre) {
        return livreRepository.findByTitre(titre);
    }

    @Override
    public Optional<Livre> getLivreById(long id) {
        return livreRepository.findById(id);
    }


    @Override
    public void addLivre(String user, String auteur, String titre, int nombre_disponible, boolean reservation_disponible, boolean isDone) {
        livreRepository.save(new Livre( user,auteur, titre, nombre_disponible, reservation_disponible, isDone));
    }

    @Override
    public void saveLivre(Livre livre) {
        livreRepository.save(livre);
    }
}
