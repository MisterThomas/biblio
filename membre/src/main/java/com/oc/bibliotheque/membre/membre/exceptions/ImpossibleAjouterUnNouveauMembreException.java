package com.oc.bibliotheque.membre.membre.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ImpossibleAjouterUnNouveauMembreException extends RuntimeException {
    public ImpossibleAjouterUnNouveauMembreException(String membre) {
        super(membre);
    }
}
