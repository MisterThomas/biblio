package com.oc.bibliotheque.membre.membre.controller;


import com.oc.bibliotheque.membre.membre.model.Membre;
import com.oc.bibliotheque.membre.membre.repository.MembreRepository;
import com.oc.bibliotheque.membre.membre.service.MembreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MembreController {


    @Autowired
    MembreRepository membreRepository;



    @Autowired
    MembreService membreService;



    @GetMapping("/api/listeMembres")
    public List<Membre> listeMembres(){
        List<Membre> membres = membreRepository.findAll();
        return membres;
    }


    @GetMapping("/api/listeMembreMail")
    public List <Membre> listeMembreMail(@RequestParam(value = "username", required = false) String username){
         username = "test2";
        List<Membre> membres = membreService.getMembreByUsername(username);
        return membres;
    }



    @GetMapping("/api/membre/{username}")
    public Membre usernameMembre(@PathVariable("username") String username){
        return  membreService.findByUsername(username);
    }


    @GetMapping("/api/membre/compte/{id}")
    public Optional<Membre> compte(@PathVariable("id") long id) {
        return membreRepository.findById(id);
    }



}
