package com.clientui.proxies;


import com.clientui.beans.MembreBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Service("proxyMenbre")
//@FeignClient(name = "zuul-server")
//@RibbonClient(name="membre")
@FeignClient(name="membre" , url="localhost:8082")
public interface MembreProxy {


   @GetMapping("/api/listeMembres")
     List<MembreBean> listeMembres();


    @GetMapping(value="/api/membre/{username}")
     MembreBean usernameMembre(@PathVariable("username") String username);


    @GetMapping("api/membre/compte/{id}")
     MembreBean compte(@RequestBody MembreBean membre, @PathVariable("id") int id);
}
