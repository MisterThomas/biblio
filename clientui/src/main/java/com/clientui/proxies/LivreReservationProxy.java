package com.clientui.proxies;


import com.clientui.beans.EmpruntBean;
import com.clientui.beans.LivreBean;
import com.clientui.beans.MembreBean;
import feign.Param;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Service("proxyLivreReservation")
//@FeignClient(name = "zuul-server")
//@RibbonClient(name="livre-reservation")
@FeignClient(name="livre-reservation" , url="localhost:8084")
public interface LivreReservationProxy {



    @GetMapping("/api/listeEmprunts")
    List<EmpruntBean> listeEmprunts();

   @GetMapping("/api/listeEmpruntsEncours")
  List<EmpruntBean> listeEmpruntsEncours(@RequestParam(value = "username") String username);

/*    @GetMapping("/api/listeEmpruntsEncours")
     List<EmpruntBean> listeEmpruntsAll(String username);*/

    @GetMapping("/api/livre/listelivres")
    List<LivreBean> listeLivres();

    @GetMapping("/api/Livre/listeLivres/Auteur")
    public List<LivreBean> listeLivresAuteur(@RequestParam("auteur") String auteur);


    @GetMapping("/api/Livre/listeLivres/Titre")
    public List<LivreBean> listeLivresTitre(@RequestParam("titre") String titre);


    @GetMapping(value="/api/Livre/{id}")
    Optional<LivreBean> detailLivre(@PathVariable int id);



    @PostMapping("/api/livre/listelivres/recherche")
     List<LivreBean> listeLivresAuteur(@RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre);


    @PutMapping("/api/listeEmpruntsEncours/upade-emprunt/{empruntId}")
     EmpruntBean prolongerEmprunt(@PathVariable("empruntId") long empruntId);


}
