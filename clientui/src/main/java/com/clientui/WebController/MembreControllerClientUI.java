package com.clientui.WebController;

import com.clientui.config.WebSecurityConfig;
import com.clientui.proxies.MembreProxy;
import com.clientui.services.MembreClientUIService;
import com.clientui.validator.MembreValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class MembreControllerClientUI {



    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    MembreClientUIService membreClientUIService;

    @Autowired
    private MembreValidator membreValidator;


    @Autowired
    MembreProxy membreProxy;



    public String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }


    @GetMapping("/login")
    public String registration(Model model) {


        //  model.addAttribute("membreForm", new Membre());

        return "login";
    }



    @GetMapping("/appLogout")
    public String logout(Model model) {


        return "redirect:/login";

    }


    @GetMapping("/compte")
    public String compte(Model model) {
        return "compte";
    }


    @GetMapping("/accueil")
    public String acceuil() {
        return "accueil";
    }
}
