package com.clientui.WebController;


import com.clientui.beans.EmpruntBean;
import com.clientui.beans.LivreBean;
import com.clientui.beans.MembreBean;
import com.clientui.proxies.LivreReservationProxy;
import com.clientui.proxies.MembreProxy;
import feign.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class LivreControllerClientUI {


@Autowired
    LivreReservationProxy livreReservationProxy;

@Autowired
    MembreProxy membreProxy;



    public String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }

/*    @GetMapping("/api/listeLivres")
            public String livres(ModelMap modelMap,@RequestBody List<LivreBean> livres){

          livreReservationProxy.listeLivres((LivreBean) livres);

        modelMap.addAttribute("livres", livres);
        return "listelivre";

    }*/


    @GetMapping("/listelivres")
    public String listelivres(ModelMap modelMap){

       List<LivreBean>  livres = livreReservationProxy.listeLivres();
        modelMap.addAttribute("livres", livres);
        return "ListeLivres";
    }



    @PostMapping("/listelivres")
    public String listelivres(@Param("search") String search,ModelMap modelMap){
        //    auteur = "anthony horowtiz";
        //     titre = "l ile du crane";
        List<LivreBean> livres = livreReservationProxy.listeLivresAuteur(search, search);
        modelMap.addAttribute("livres", livres);

        if (search == null){
            livres = livreReservationProxy.listeLivres();
            modelMap.addAttribute("livres", livres);
            return "ListeLivres";
        }
        return "ListeLivres";
    }






  /*  @GetMapping("/api/Livre/listeLivres/recherche")
    public String recupererlivres(ModelMap modelMap, @RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre ){

       List<LivreBean> livre = livreReservationProxy.recupererlivres(auteur, titre);

        modelMap.addAttribute("livres", livre);

        return "showlivres";

    }
*/







}
