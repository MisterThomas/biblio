package com.clientui.WebController;


import com.clientui.beans.EmpruntBean;
import com.clientui.beans.LivreBean;
import com.clientui.beans.MembreBean;
import com.clientui.proxies.LivreReservationProxy;
import com.clientui.proxies.MembreProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Controller
public class EmpruntControllerClientUI {

    @Autowired
    LivreReservationProxy livreReservationProxy;

    @Autowired
    MembreProxy membreProxy;


    public String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }



    @GetMapping("/listereservation/{username}")
    public String listereservation(ModelMap modelMap, @PathVariable("username") String username){

         username = getLoggedInUserName(modelMap);
    //    List emprunt = livreReservationProxy.listeEmpruntsAll(username);
        modelMap.addAttribute("emprunts", livreReservationProxy.listeEmpruntsEncours(username));
   //     emprunt.toString();
        return "ListeReservation";
    }



    @GetMapping("/api/listereservation/{username}/upade-reservation/{reservationId}")
    public String showUpdateEmprunt(ModelMap modelMap,@PathVariable("reservationId") int reservationId, ModelMap model,@PathVariable("username") String username) {

            username = getLoggedInUserName(modelMap);
            EmpruntBean empruntBean = livreReservationProxy.prolongerEmprunt(reservationId);
           // empruntBean = livreReservationProxy.showUpdateReservation(reservationId, empruntBean);
          // String email = empruntBean.getUsername();
           empruntBean.setUsername(getLoggedInUserName(model));

        return "redirect:/listereservation/{username}";
    }







}
